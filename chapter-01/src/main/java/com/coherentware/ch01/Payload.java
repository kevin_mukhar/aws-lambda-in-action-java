package com.coherentware.ch01;

public class Payload {
	public Payload() {
	}

	public Payload(String greet, String name) {
		super();
		this.greet = greet;
		this.name = name;
	}

	public String greet;
	public String name;

	public String getGreet() {
		return greet;
	}

	public void setGreet(String greet) {
		this.greet = greet;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
