package com.coherentware.ch01;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

public class HelloWithPayload {
	private static final String NL = System.getProperty("line.separator");

	public String myHandler(Payload payload, Context context) {
		LambdaLogger logger = context.getLogger();

		String greet = "Hello";
		String name = "World";

		if (payload.greet != null && !"".equals(payload.greet)) {
			greet = payload.greet;
		}

		if (payload.name != null && !"".equals(payload.name)) {
			name = payload.name;
		}

		String response = greet + ", " + name.trim() + "!";
		logger.log(response + NL);

		return response;
	}
}
