package com.coherentware.ch01;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

public class HelloSimple {
	private static final String NL = System.getProperty("line.separator");

	public String myHandler(String payload, Context context) {
		LambdaLogger logger = context.getLogger();

		String greet = "Hello";
		String name = "World";

		if (payload != null && !"".equals(payload)) {
			name = payload.trim();
		}

		String response = greet + ", " + name + "!";
		logger.log(response + NL);

		return response;
	}

}
